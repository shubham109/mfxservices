const _dd = require("./datasetdefn");
const moment = require("moment");
const _ = require("lodash");
const dbconfig = require("./dbconfig");
const oracledb = require("oracledb");

module.exports = (app) => {
    _app = app;
    const addRecord = async (Obj) => {
        let fields = [], vals = [], tablename = Obj.tablename, keys = [], vdata = {}, connection = null, wclause = [], wobj = {};
        logger.info(tablename)
        logger.debug("Req obj" + JSON.stringify(Obj))
        if (!Obj.tablename || !Obj.JString) {
            logger.error("Insufficient data");
            return { "status": "unsuccess", "error": "Insufficient data (Tablename/JString)" }
        }
        vdata = Object.assign({}, Obj.JString);
        fields = Object.keys(Obj.JString)
        _.each(fields, function (field) {
            if (_dd[tablename][field] && _dd[tablename][field]["type"] == "DATE") {
                vdata[field] = (vdata[field]) ? moment(vdata[field], dbconfig.DATE_FORMAT).toDate() : ""
            }
            vals.push((_dd[tablename][field] && _dd[tablename][field]["sequence"]) ? _dd[tablename][field]["sequence"] + ".nextval" : ":" + field);
            if (_dd[tablename][field] && _dd[tablename][field]["iskey"]) {
                keys.push(field);
                vdata[field] = (_dd[tablename][field]["sequence"]) ? { dir: oracledb.BIND_OUT } : { dir: oracledb.BIND_INOUT, "val": Obj.JString[field] };
                wclause.push(field + "=:" + field);
                // wobj[field] = vdata[field];
            }
        });
        let insQry = "Insert into " + tablename + " (" + fields + ") values(" + vals.join(",") + ") returning " + keys.join(",") + " into " + ":" + keys.join(",:");
        logger.debug("iQry:" + insQry)
        try {
            let options = {}
            options.autoCommit = false;
            logger.debug("qry opt : " + JSON.stringify(options))
            connection = Obj.connection ? Obj.connection : await DB["oraclePool"].getConnection();
            let result = await connection.execute(insQry, vdata, options);
            result.status = "success"
            result.msg = "Record created successfully"
            logger.info("Result : " + JSON.stringify(result));
            wobj["sys_userid"] = Obj.userObj.user_id;
            for (let m in result.outBinds)
                wobj[m] = "" + result.outBinds[m][0];

            insQry = "INSERT INTO audit_" + tablename + " SELECT a.*,'create' sys_auditreason, :sys_userid, sysdate sys_auditdate,'create' sys_operation,1 sys_audit_id FROM " + tablename + " a WHERE " + wclause.join(" and ") + "";
            logger.debug("wObj : " + JSON.stringify(wobj))
            logger.info("Insert qry: " + insQry);
            let aresult = await connection.execute(insQry, wobj, options);
            logger.info("Create Audit " + JSON.stringify(aresult));
            let commit = !Obj.connection ? await connection.commit() : ""
            return result;
        } catch (error) {
            logger.error(error.stack)
            return { "status": "unsuccess", "error": "Error while creating record" }
        }
        finally {
            if (!Obj.connection) {
                logger.info(`Finally Conn close`)
                await connection.close()
            }
        }
    }
    const updateRecord = async (Obj) => {
        let wclause = [], vals = [], tablename = Obj.tablename;
        logger.debug("Req obj" + JSON.stringify(Obj))
        if (!Obj.tablename || !Obj.JString) {
            logger.error("Insufficient data");
            return { "status": "unsuccess", "error": "Insufficient data (Tablename/JString)" }
        }
        if (!_dd[tablename]) {
            logger.error("Data definition not found.");
            return { "status": "unsuccess", "error": "Data definition not found(Tablename/JString)" }
        }
        let vdata = Object.assign({}, Obj.JString);
        for (let k in vdata) {
            if (_dd[tablename][k] && _dd[tablename][k]["iskey"]) {
                if (!vdata[k]) {
                    logger.error("Key value is blank");
                    return { "status": "unsuccess", "error": "Key value cannot be blank." }
                }
                wclause.push(k + "=:" + k);
                continue;
            }
            if (_dd[tablename][k] && _dd[tablename][k]["type"] && _dd[tablename][k]["type"] == "DATE")
                vdata[k] = vdata[k] ? moment(vdata[k], dbconfig.DATE_FORMAT).toDate() : ""
            vals.push(k + "=:" + k);
        }
        vdata.sys_userid = Obj.userObj.user_id;
        let uQry = "Begin INSERT INTO audit_" + tablename + " SELECT a.*,'update' sys_auditreason, :sys_userid, sysdate sys_auditdate,'update' sys_operation,1 sys_audit_id FROM " + tablename + " a WHERE " + wclause.join(" and ") + ";update " + tablename + " set " + vals.join(",") + " where " + wclause.join(" and ") + ";End;";
        logger.info("Update Query " + uQry);
        let connection = null
        try {
            let options = {}
            options.autoCommit = Obj.connection ? false : true;
            logger.debug("qry opt : " + JSON.stringify(options))
            connection = Obj.connection ? Obj.connection : await DB["oraclePool"].getConnection();
            let result = await connection.execute(uQry, vdata, options);
            logger.info("Result : " + result.rowsAffected)
            return { "status": "success", "msg": "Record updated successfully" };
        } catch (error) {
            logger.error(error.stack);
            return { "status": "unsuccess", "error": "Error while updating record" }
        }
        finally {
            if (!Obj.connection) {
                logger.info(`Finally Conn close`)
                await connection.close()
            }
        }
    }
    const deleteRecord = async (Obj) => {
        let wclause = [], tablename = Obj.tablename, vdata = {};
        logger.debug("Req obj" + JSON.stringify(Obj))
        if (!Obj.tablename || !Obj.JString) {
            logger.error("Insufficient data");
            return { "status": "unsuccess", "error": "Insufficient data (Tablename/JString)" }
        }
        if (!_dd[tablename]) {
            logger.error("Data definition not found.");
            return { "status": "unsuccess", "error": "Data definition not found(Tablename/JString)" }
        }
        vdata = Object.assign({}, Obj.JString);
        vdata.sys_userid = Obj.userObj.user_id;
        for (let k in vdata) {
            if (_dd[tablename][k] && _dd[tablename][k]["iskey"]) {
                if (!vdata[k]) {
                    logger.error("Key value is blank");
                    return { "status": "unsuccess", "error": "Key value cannot be blank." }
                }
                wclause.push(k + "=:" + k);
                continue;
            }
        }
        let uQry = "INSERT INTO audit_" + tablename + " SELECT a.*,'delete' sys_auditreason, :sys_userid, sysdate sys_auditdate,'delete' sys_operation,1 sys_audit_id FROM " + tablename + " a WHERE " + wclause.join(" and ") + ";"
        uQry = Obj.isSoftDelete ? "BEGIN update " + tablename + " set deleted ='Y' where " + wclause.join(" and ") + ";" + uQry + "End;" : "BEGIN " + uQry + "delete from " + tablename + " where " + wclause.join(" and ") + ";End;";
        logger.info("Delete Query " + uQry);
        let connection = null
        try {
            let options = {}
            options.autoCommit = Obj.connection ? false : true;
            logger.debug("qry opt : " + JSON.stringify(options))
            connection = Obj.connection ? Obj.connection : await DB["oraclePool"].getConnection();
            let result = await connection.execute(uQry, vdata, options);
            logger.info(result.rowsAffected)
            return { "status": "success", "msg": "Record deleted successfully" };
        } catch (error) {
            logger.error(error.stack);
            return { "status": "unsuccess", "error": "Error while deleting record" }
        }
        finally {
            if (!Obj.connection) {
                logger.info(`Finally Conn close`)
                await connection.close()
            }
        }
    }
    const executeQuery = async (obj) => {
        try {
            let rObj = {}, vObj = {}, asgnOP = {}, wcl = [], result, finqry;
            logger.debug("Req obj : " + JSON.stringify(obj))
            if (!obj.query) {
                logger.error("Insufficient data");
                return { "status": "unsuccess", "error": "Insufficient data (Query)" }
            }
            obj.filter = obj.filter || []
            obj.sortby = obj.sortby || ""
            rObj = Object.assign({}, obj.filter)
            asgnOP = { "eq": "=", "noteq": "!=", "gt": ">", "lt": "<", "gteq": ">=", "lteq": "<=", "notin": "not in", "in": "in", "like": "like", "notlike": "not like" }
            for (let i in rObj) {
                if (!rObj[i].field) {
                    logger.debug("field" + i + " is empty")
                    return (obj.isSQL) ? "" : [];
                }
                rObj[i].asgn = rObj[i].asgn || "eq";
                if (rObj[i].type == "DATE") {
                    wcl.push(rObj[i].field + asgnOP[rObj[i].asgn] + ' :' + rObj[i].field)
                    vObj[rObj[i].field] = moment(rObj[i].value, dbconfig.DATE_FORMAT).toDate();
                }
                else if (asgnOP[rObj[i].asgn] == "in" || asgnOP[rObj[i].asgn] == "not in") {
                    wcl.push(rObj[i].field + " " + asgnOP[rObj[i].asgn] + " ('" + rObj[i].value.join("','") + "')")
                }
                else if (asgnOP[rObj[i].asgn] == "like" || asgnOP[rObj[i].asgn] == "not like") {
                    wcl.push("lower(" + rObj[i].field + ') ' + asgnOP[rObj[i].asgn] + ' lower(:' + rObj[i].field + ")")
                    vObj[rObj[i].field] = rObj[i].value + "%"
                }
                else {
                    wcl.push(rObj[i].field + asgnOP[rObj[i].asgn] + ' :' + rObj[i].field)
                    vObj[rObj[i].field] = rObj[i].value
                }
            }
            finqry = obj.query + ((wcl.length > 0) ? "and " + wcl.join(" and ") : "") + " " + obj.sortby
            if (obj.isSQL)
                return finqry;

            if(obj.page && obj.page.start !=-1 && obj.page.limit !=-1)
            {
                finqry = await getPageWiseQuery(finqry);
                vObj["STARTNO"] =  obj.page.start
                vObj["LIMITNO"] =  _.add(parseInt(obj.page.start),parseInt(obj.page.limit)).toString()
            }
            logger.info("Query :" + finqry)
            logger.debug("Bind obj : " + JSON.stringify(vObj))
            result = await execSQL(finqry, vObj);
            return result.rows ? result.rows : result;
        }
        catch (error) {
            logger.error(error.stack);
            return { "status": "unsuccess", "error": "Error while executing given query" }
        }
    }
    const incrementseq = async (tablename, colname) => {
        try {
            let seqQry, updateQry, result, resultU
            seqQry = `select (seq+1) seq from sequencetable where UPPER(context)=UPPER(:context) `
            result = await execSQL(seqQry, { context: tablename + "." + colname })
            updateQry = "UPDATE sequencetable set seq=seq+1 where upper(context)=upper(:context)"
            resultU = await execSQL(updateQry, { context: tablename + "." + colname })
            logger.debug(JSON.stringify(resultU))
            logger.debug("Sequence : " + result.rows[0].SEQ)
            return result.rows ? { "status": "success", "sequence": result.rows[0].SEQ } : result
        }
        catch (error) {
            logger.error(error.stack);
            return { "status": "unsuccess", "error": "Error while updating sequence" }
        }
    }
    const getOConnection = async () => {
        return await DB["oraclePool"].getConnection();
    }
    const execSQL = async (qry, vbinding) => {
        let conn = null, result;
        if (!qry) {
            logger.error("Query is blank");
            return { "status": "unsuccess", "error": "Query cannot be blank" };
        }
        try {
            conn = await getOConnection();
            result = (vbinding) ? await conn.execute(qry, vbinding) : await conn.execute(qry);
            return result;
        } catch (error) {
            console.log(error.stack);
            logger.error("Error while executing the query");
            return { "status": "unsuccess", "error": "Error while executing the query." }
        }
        finally {
            if (conn) { conn.close(); conn = null; }
        }
    }
    const getPageWiseQuery = async (qry)=> {
        let vSql = [];
        let strQry = qry||""
        if(strQry=="")
            return ""

        vSql[vSql.length]="SELECT * FROM("
        vSql[vSql.length]="SELECT a.* , ";
        vSql[vSql.length]="Row_Number() over(ORDER BY 1) sr_no,";
        vSql[vSql.length]="count(1) over(ORDER BY 1) rc_internal"; 
        vSql[vSql.length]="FROM ( ";
        vSql[vSql.length]=strQry;
        vSql[vSql.length]=")a )";
        vSql[vSql.length]="WHERE sr_no >= :STARTNO AND sr_no <= :LIMITNO ";	
        return vSql.join(" ");
    }

    return { "addRecord": addRecord, "updateRecord": updateRecord, "deleteRecord": deleteRecord, "executeQuery": executeQuery, "incrementseq": incrementseq, "getOConnection": getOConnection, "execSQL": execSQL, "getPageWiseQuery": getPageWiseQuery}
}