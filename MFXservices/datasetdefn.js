module.exports = {
    "wm_client_master": {
        "CLIENT_ID": { "type": "STRING", "iskey": true }
        , "DOB": { "type": "DATE" }
        , "RBI_EXPIRY_DATE": { "type": "DATE" }
        , "DATEOFAUTH": { "type": "DATE" }
        , "HOLDMAIL_FROMDATE": { "type": "DATE" }
        , "HOLDMAIL_TODATE": { "type": "DATE" }
        , "INACTIVE_FROMDATE": { "type": "DATE" }
        , "INACTIVE_TODATE": { "type": "DATE" }
        , "DATEOFUPDATION": { "type": "DATE" }
        , "ANNUAL_INCOME": { "type": "NUMBER" }
        , "NET_WORTH": { "type": "NUMBER" }
        , "DOI": { "type": "DATE" }
        , "DOE": { "type": "DATE" }
        , "CREATED_ON": { "type": "DATE" }
        , "SENIOR_CITIZEN_WEF": { "type": "DATE" }
        , "CL_RISK_PROFILED_DATE": { "type": "DATE" }
        , "ENTITYCODE": { "type": "NUMBER" }
        , "DATE_OF_JOIN": { "type": "DATE" }
        , "NO_OF_DEPENDENTS": { "type": "NUMBER" }
        , "PERMANENCY_DATE": { "type": "DATE" }
        , "EFF_DATE_OF_REG": { "type": "DATE" }
        , "NEXT_REVIEW_DATE": { "type": "DATE" }
        , "AMLA_SCORE": { "type": "NUMBER" }
        , "CL_RISK_SCORE": { "type": "NUMBER" }
        , "CL_RISK_MODEL_ID": { "type": "NUMBER" }
    }
    , "wm_client_ac_master": {
        "PMS_ACNO": { "type": "STRING", "iskey": true }
        , "DATEOFUPDATION": { "type": "DATE" }
        , "DATEOFAUTH": { "type": "DATE" }
        , "UNITS": { "type": "NUMBER" }
        , "UNITS_ON": { "type": "DATE" }
        , "RISK_PROFILED_DATE": { "type": "DATE" }
        , "CREATED_ON": { "type": "DATE" }
        , "NEXT_FEE_ACCRUAL_DATE": { "type": "DATE" }
        , "NEXT_FEE_PAYMENT_DATE": { "type": "DATE" }
        , "REBATE_PERC": { "type": "NUMBER" }
        , "RB_START_DATE": { "type": "DATE" }
        , "INC_DIST_PERC": { "type": "NUMBER" }
        , "CUSTODIAN_FEE_PERC": { "type": "NUMBER" }
        , "CL_RISK_SCORE": { "type": "NUMBER" }
        , "CL_RISK_MODEL_ID": { "type": "NUMBER" }
    }
    , "family_members": {
        "EARNING": { "type": "NUMBER" }
        , "DATE_OF_BIRTH": { "type": "DATE" }
        , "ID": { "type": "NUMBER", "iskey": true, "sequence": "seq_family_mem" }
        , "CREATED_ON": { "type": "DATE" }
        , "UPDATED_ON": { "type": "DATE" }
    }
    , "basic_profile": {}
    , "client_goals": {
        "ID": { "type": "NUMBER", "iskey": true, "sequence": "seq_client_goals" }
        , "ASSESSMENT_YEAR": { "type": "NUMBER" }
        , "CURRENT_AGE": { "type": "NUMBER" }
        , "MONTHLY_EXPENSE": { "type": "NUMBER" }
        , "RETIREMENT_AGE": { "type": "NUMBER" }
        , "LIFE_EXPECTANCY": { "type": "NUMBER" }
        , "YEARS_IN_RETIREMENT": { "type": "NUMBER" }
        , "INVESTMENT_HORIZON": { "type": "NUMBER" }
        , "INFLATION": { "type": "NUMBER" }
        , "GOAL_PRIORITY": { "type": "NUMBER" }
        , "SURPLUS_INCOME_ALLOCATION": { "type": "NUMBER" }
        , "INVESTIBLE_SURPLUS": { "type": "NUMBER" }
        , "INVESTIBLE_SURPLUS_GROWTH_RATE": { "type": "NUMBER" }
        , "ALLOCATION_LEFT": { "type": "NUMBER" }
        , "SYSTEMATIC_INVESTMENT": { "type": "NUMBER" }
        , "ANNUAL_INCREASE": { "type": "NUMBER" }
        , "START_INVEST_LUMP_SUM_CONTRI": { "type": "NUMBER" }
        , "EXPENSE_AT_RETIREMENT_ANNUAL": { "type": "NUMBER" }
        , "EXPENSE_AT_RETIREMENT_MONTH": { "type": "NUMBER" }
        , "NO_OF_INSTALMENTS": { "type": "NUMBER" }
        , "INVESTMENT_HORIZON_MONTH": { "type": "NUMBER" }
        , "INVEST_HORI_GOAL_FULLFIL_YEAR": { "type": "NUMBER" }
        , "NO_OF_MONTHS": { "type": "NUMBER" }
        , "CORPUS_REQUIRED_IN_TODAYS_TERM": { "type": "NUMBER" }
        , "EMERGENCY_CORPUS_REQUIRED": { "type": "NUMBER" }
        , "RATE_OF_RETURN": { "type": "NUMBER" }
        , "RATE_OF_RETURN_POST_RETIREMENT": { "type": "NUMBER" }
        , "TOTAL_REQ_CORPUS_RETIERE": { "type": "NUMBER" }
        , "TOTAL_REQUIRED_TARGET_CORPUS": { "type": "NUMBER" }
        , "VOLATILITY": { "type": "NUMBER" }
        , "PRESENT_VALUE_OF_TOTAL_INVEST": { "type": "NUMBER" }
        , "FUTURE_VOT_INVESTMENT_EST_CORP": { "type": "NUMBER" }
        , "SURPLUS_OR_DEFICIT": { "type": "NUMBER" }
        , "SURPLUS_OR_DEFICIT_PERC": { "type": "NUMBER" }
        , "PRESENT_VOT_INVEST_MIN_RETURN": { "type": "NUMBER" }
        , "PRESENT_VOT_INVEST_MAX_RETURN": { "type": "NUMBER" }
        , "FUTURE_VOT_INVEST_MIN_VALUE": { "type": "NUMBER" }
        , "FUTURE_VOT_INVEST_MAX_VALUE": { "type": "NUMBER" }
        , "MINIMUM_SURPLUS": { "type": "NUMBER" }
        , "MINIMUM_SURPLUS_DEFICIT_PERC": { "type": "NUMBER" }
        , "MAXIMUM_SUPLUS": { "type": "NUMBER" }
        , "MAXIMUM_SUPLUS_OR_DEFICIT_PERC": { "type": "NUMBER" }
        , "EXPENSE_TO_BE_EARMARKED": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
        , "SURPLUS_ALLOCATION": { "type": "NUMBER" }
        , "TARGET_DATE": { "type": "DATE" }
        , "PRODUCT": { "type": "NUMBER" }
    },
    "wm_subac_typ_mast": {
        "ACCT_CODE": { "type": "STRING", "iskey": true }
        , "SUB_AC_TYPE": { "type": "STRING", "iskey": true }
        , "MIN_AMOUNT": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
        , "AUTHORIZATION_STATUS": { "type": "NUMBER" }
        , "HOLDING_PERIOD_DAYS": { "type": "NUMBER" }
        , "HOLDING_PERIOD_MONTHS": { "type": "NUMBER" }
        , "TRUST_FEE": { "type": "NUMBER" }
        , "MAX_AMT": { "type": "NUMBER" }
    },
    "modelportfolio": {
        "MODELPORT_CODE": { "type": "NUMBER", "iskey": true, "sequence": "seq_modelportfolio" }
        , "EFFECTIVEFROM": { "type": "DATE" }
        , "ACTIVE": { "type": "NUMBER" }
        , "DATEOFUPDATION": { "type": "DATE" }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
        , "RATING": { "type": "NUMBER" }
        , "RISK_SCORE": { "type": "NUMBER" }
        , "BENCHMARK_RETURN": { "type": "NUMBER" }
        , "PROJECTED_RETURN": { "type": "NUMBER" }
        , "ALPHA": { "type": "NUMBER" }
        , "DOWNSIDE_RISK": { "type": "NUMBER" }
        , "PRODUCT_RETURN": { "type": "NUMBER" }
    },
    "modelport_details": {
        "MODELPORT_CODE": { "type": "NUMBER", "iskey": true }
        , "SECURITYSYMBOL": { "type": "STRING", "iskey": true }
        , "SRNO": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "wm_product_master": {
        "PRODUCT_CODE": { "type": "STRING", "iskey": true }
        , "DATEOFAUTH": { "type": "DATE" }
        , "INDEX_CODE": { "type": "NUMBER" }
        , "INDEX_FLAG": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
    },
    "wm_cl_grps": {
        "CLIENT_GROUPID": { "type": "STRING", "iskey": true }
        , "DATEOFUPDATION": { "type": "DATE" }
        , "DATEOFAUTH": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "risk_model_questions": {
        "RISK_ID": { "type": "NUMBER" }
        , "OPTION_ID": { "type": "NUMBER" }
    },
    "risk_model_answers": {
        "OPTION_ID": { "type": "NUMBER" }
        , "SRNO": { "type": "NUMBER" }
        , "WEIGHT": { "type": "NUMBER" }
    },
    "rebalance_asset_allocation": {
        "PORTCODE": { "type": "STRING", "iskey": true }
        , "WEIGHTAGE": { "type": "NUMBER", "iskey": true }
        , "MODELPORTFOLIO": { "type": "STRING", "iskey": true }
    },
    "wm_document_master": {
        "DOC_CODE": { "type": "NUMBER", "iskey": true }
        , "MANDATORY": { "type": "NUMBER" }
        , "ACTIVE": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
        , "AUTHORIZATION_STATUS": { "type": "NUMBER" }
        , "EFFECTIVE_DATE": { "type": "DATE" }
    },
    "wm_document_list": {
        "DOC_CODE": { "type": "NUMBER", "iskey": true, "sequence": "seq_in_document_master" }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
        , "authorization_status": { "type": "NUMBER" }
    },
    "wm_risk_master": {
        "RISK_ID": { "type": "STRING", "iskey": true }
        , "CREATED_ON": { "type": "DATE" }
        , "UPDATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "wm_riskprofile_master": {
        "RISK_ID": { "type": "NUMBER", "iskey": true }
        , "PROFILE_ID": { "type": "NUMBER", "iskey": true, "sequence": "seq_risk_profile" }
        , "WEIGHT_MIN": { "type": "NUMBER" }
        , "WEIGHT_MAX": { "type": "NUMBER" }
        , "SECURITY_RISK_SCORE": { "type": "NUMBER" }
        , "PRODUCT_RISK_SCORE": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
        , "UPDATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "risk_asset": {
        "RISK_ID": { "type": "NUMBER", "iskey": true }
        , "PROFILE_ID": { "type": "NUMBER", "iskey": true }
        , "ASSET_CLASS": { "type": "STRING", "iskey": true }
        , "MIN_PERCENT": { "type": "NUMBER" }
        , "MAX_PERCENT": { "type": "NUMBER" }
        , "TARGET_PERCENT": { "type": "NUMBER" }
    },
    "sip_entry": {
        "ENTRY_ID": { "type": "NUMBER", "iskey": true, "sequence": "seq_sip_enrty_id" }
        , "CONTRIB_START_DATE": { "type": "DATE" }
        , "CONTRIB_AMOUNT": { "type": "NUMBER" }
        , "NEXT_CONTRIB_DATE": { "type": "DATE" }
        , "CREATED_ON": { "type": "DATE" }
        , "UPDATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "eq_preorder": {
        "PREORDER_ID": { "type": "NUMBER", "iskey": true, "sequence": "preorder_id_seq" }
        , "ORDERDATE": { "type": "DATE" }
        , "QUANTITY": { "type": "NUMBER" }
        , "RATE": { "type": "NUMBER" }
        , "MAX_RATE": { "type": "NUMBER" }
        , "ORDERVALIDDATE": { "type": "DATE" }
        , "IR_ID": { "type": "NUMBER" }
        , "AMOUNT": { "type": "NUMBER" }
        , "SQUAREOFF": { "type": "NUMBER" }
        , "IN_PORTCODE": { "type": "NUMBER" }
        , "OUT_PORTCODE": { "type": "NUMBER" }
        , "BESTPRICE": { "type": "NUMBER" }
        , "MANDATEID": { "type": "NUMBER" }
        , "ORDER_REFID": { "type": "NUMBER" }
        , "ISPRODUCT": { "type": "NUMBER" }
        , "YIELD": { "type": "NUMBER" }
        , "STATUS": { "type": "NUMBER" }
        , "IS_AMOUNT_BASED_ORDER": { "type": "NUMBER" }
        , "PREMANDATE_ID": { "type": "NUMBER" }
        , "PRICE_FROM": { "type": "NUMBER" }
        , "PRICE_TO": { "type": "NUMBER" }
        , "YIELD_FROM": { "type": "NUMBER" }
        , "YIELD_TO": { "type": "NUMBER" }
        , "PCY_MTM_VALUE": { "type": "NUMBER" }
        , "PCY_ORDER_AMOUNT": { "type": "NUMBER" }
        , "CONVERSION_RATE": { "type": "NUMBER" }
        , "FOLIONO": { "type": "NUMBER" }
        , "FD_DAYCOUNT": { "type": "NUMBER" }
        , "MAT_ADDTNL_AMT": { "type": "NUMBER" }
    },
    "wm_client_documents": {
        "SRNO": { "type": "NUMBER", "iskey": true, "sequence": "doc_srno_seq" }
        , "RECEIVED_DATE": { "type": "DATE" }
        , "EXPECTED_DATE": { "type": "DATE" }
        , "EXPIRY_DATE": { "type": "DATE" }
        , "FILEDATA": { "type": "BLOB" }
    },
    "wm_client_ac_documents": {
        "SRNO": { "type": "STRING", "iskey": true, "sequence": "doc_acc_srno_seq" }
        , "DOC_CODE": { "type": "STRING", "iskey": true }
        , "RECEIVE_STATUS": { "type": "STRING", "iskey": true }
        , "REF_ID": { "type": "STRING", "iskey": true }
    },
    "financial_plan": {
        "FINANCIAL_ID": { "type": "NUMBER", "iskey": true, "sequence": "seq_financial_plan" }
        , "STARTDATE": { "type": "DATE" }
        , "COMPLETION_PERCENT": { "type": "NUMBER" }
        , "ACCEPTANCE_DATE": { "type": "DATE" }
        , "CURRENT_INVESTMENTS": { "type": "NUMBER" }
        , "COMMITED_INVESTMENT": { "type": "NUMBER" }
        , "NEXT_REVIEW_DATE": { "type": "DATE" }
    },
    "wm_acc_document_master": {
        "DOC_CODE": { "type": "STRING", "iskey": true }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
        , "AUTHORIZATION_STATUS": { "type": "NUMBER" }
        , "EFFECTIVE_DATE": { "type": "DATE" }
        , "ACTIVE": { "type": "NUMBER" }
    },
    "wm_trans_document_type": {
        "TRANS_DOC_ID": { "type": "NUMBER", "iskey": true, "sequence": "seq_trans_doc_id" }
        , "MANDATORY": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
        , "AUTHORIZATION_STATUS": { "type": "NUMBER" }
    },
    "wm_goals_obj_priorities": {
        "CLIENT_ID": { "type": "NUMBER", "iskey": true }
        , "CREATED_ON": { "type": "DATE" }
        , "UPDATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "custodian_account_master": {
        "REC_ID": { "type": "NUMBER", "iskey": true }
        , "CUSTODIAN_CODE": { "type": "NUMBER" }
        , "CREATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "cms_bankmaster": {
        "REC_ID": { "type": "NUMBER", "iskey": true }
        , "LOCATION": { "type": "NUMBER" }
    },
    "security_restriction": {
        "RECORD_ID": { "type": "STRING", "iskey": true, "sequence": "seq_security_restriction" }
        , "CREATED_ON": { "type": "DATE" }
        , "UPDATED_ON": { "type": "DATE" }
        , "AUTHORIZATIONDATETIME": { "type": "DATE" }
    },
    "wm_cl_ac_add_appl": {
        "CLIENT_ACCOUNT_ID": { "type": "NUMBER", "iskey": true }
    },
    "wm_client_ac_fee_map": {
        "CLIENT_ACCOUNT_ID": { "type": "NUMBER", "iskey": true }
    },
    "if_client_ac_costing": {
        "PORTCODE": { "type": "NUMBER", "iskey": true }
    },
    "wm_trans_documents": {
        "SRNO": { "type": "NUMBER", "iskey": true, "sequence": "trans_doc_srno_seq" }
        , "RECEIVED_DATE": { "type": "DATE" }
        , "EXPECTED_DATE": { "type": "DATE" }
        , "EXPIRY_DATE": { "type": "DATE" }
        , "FILEDATA": { "type": "BLOB" }
    }
}