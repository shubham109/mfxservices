const fetch = require('node-fetch');
var fs = require('fs');
var config = require('../../config');
global.fetch = fetch
global.Headers = fetch.Headers;
const getDButils = require("../../dbutils");
var uuid = require('node-uuid');
module.exports = app => {
    let _app = app;
    let dbutil = getDButils(_app);
    return {
        "read": async function (req, res) {
           try {
               if(config){
                let header_my = new Headers(config.header)
                process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
                let created_on= await dbutil.execSQL(`select count(*) from mfx_gst_at where created_on >= (sysdate-6/24)`)
                        if(!created_on){
                            res.send(await dbutil.execSQL('select access_token from mfx_gst_at '))
                        }
                        else{
                var d = fetch(config.endpoint.url,{ 
                        method: 'post', 
                        headers: header_my,
                    })
                    .then(res => res.json())
                    .then(async res1=>{
                        //let date_ob = new Date();
                        var accesstoken = res1.accesstoken;
                        var refreshtoken = res1.refreshtoken;
                        var insertquery=`insert into mfx_gst_at (access_token,refresh_token,created_on) values (\'${accesstoken}\',\'${refreshtoken}\',SYSDATE)`;
                        let result =await dbutil.execSQL(insertquery);
                    if (!result.status){
                        result.status="success"
                        result.msg="Basic profile created successfully"
                        accessobj={"accesstoken":accesstoken}
                        res.send(accessobj)
                    }
                    else{
                        logger.info("failed")
                        res.send(result)
                    }
                })
                     .catch(err => logger.error(err.message));
               }
            }
               else{
                   logger.error("some Parameters are missing in config file")
                   res.send(err.message)
               }
           } catch (error) {
               logger.error(error)
               send_obj = {
                "Status": "UnSuccessful"
            }
               res.send(send_obj)
           }
        },
    "log":async (req,res)=>{
        try {
            //console.log(res)
            if(config){
             let header_my = new Headers(config.header)
             process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
             var d = fetch(config.endpoint.url,{ 
                     method: 'post', 
                     headers: header_my,
                 })
                  .then(res => res.json())
                  .then(async res1 => {
                      let str=JSON.stringify(res1)
                      let query=`insert into mfx_gst_intf_log (request_id, trans_no, sent_time, received_time, post_status, event, xml_request, xml_response,senderid,source,xml_request_raw) VALUES (1,NULL,SYSDATE,SYSDATE,'Success',NULL,NULL,\'${str}\',NULL,NULL,NULL) `
                    let result =await dbutil.execSQL(query);
                    console.log(query)
                    if(result){
                        logger.info("query executed successfully")
                        res.send(res1)
                    }
                    else{
                        logger.error("query execution Failed")
                        res.send("failed")
                    }
                  })
                  .catch(err => logger.error(err.message));
                }
        }catch (error) {
            logger.error(error)
        }
    },
    "uuid": async(req,res)=>{
        try {
            res.send(uuid.v1())
        } catch (error) {
            logger.error(error)
        }
    }
}
}