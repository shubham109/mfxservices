const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = chai;
chai.use(chaiHttp);
describe('Post / Read', function () {
    this.timeout("3000");
    it('get response', function(done) {
        chai
        .request('http://localhost:3004/authentication').post('/read')
        .set({'sessionid':'923719602972808660095992479824'})
        .set('Accept', 'Application.json')
        .end((err, res) => {
            expect(res.statusCode).to.equal(200);
             done();
        });
    })
    it('get response', function(done) {
        chai
        .request('http://localhost:3004/authentication').post('/log')
        .set({'sessionid':'923719602972808660095992479824'})
        .set('Accept', 'Application.json')
        .end((err, res) => {
            expect(res.statusCode).to.equal(200);
             done();
        });
    })
    it('get response', function(done) {
        chai
        .request('http://localhost:3004/authentication').post('/uuid')
        .set({'sessionid':'923719602972808660095992479824'})
        .set('Accept', 'Application.json')
        .end((err, res) => {
            expect(res.statusCode).to.equal(200);
             done();
        });
    })
});