const getDButils = require("../../dbutils");
const authentication =  require('../../authentication/Home/handler')
console.log(authentication.res)
module.exports = app => {
    let _app = app;
    let dbutil = getDButils(_app);
    return {
        "insert": async function (req, res) {
            try {
                let qry = "", insertqry = "", custid = "";
                logger.debug("Req obj" + JSON.stringify(req.body))
                if (!req.body.data) {
                    logger.error("Insufficient data {data}.")
                    res.json({ "error": "Insufficient data." });
                }
                custid = req.body.data.customer_id;
                let delqry = " delete from basic_profile where customer_id='" + custid + "'; ";
                logger.info("Delete qry : " + delqry);
                for (let key in req.body.data["basic_profile"]) {
                    let cat = key;
                    let catobj = req.body.data["basic_profile"][key];
                    for (let i = 0, len = catobj.length; i < len; i++) {
                        insertqry += " insert into basic_profile(customer_id, category, sub_category) values('" + custid + "', '" + key + "', '" + catobj[i]["name"] + "' ); ";
                        logger.info(" Category : " + cat + ", Sub Category : " + catobj[i]["name"]);
                    }
                }
                qry = await (" BEGIN  " + delqry + insertqry + " END; ")
                logger.info("Final qry : " + qry);
                logger.debug("Bind Obj" + JSON.stringify(req.body.data))
                let result = await dbutil.execSQL(qry);
                if (!result.status){
                    result.status="success"
                    result.msg="Basic profile created successfully"
                }
                logger.info(JSON.stringify(result));
                return res.json(result);
            }
            catch (error) {
                logger.info('{"status":"unsuccess","error":' + error + ',"msg":"Error occurred while creating basic profile" }');
                return res.json({ "status": "unsuccess", "error": "Error occurred while creating basic profile", "msg": "" });
            }
        }
    }
}
