const assert = require('assert')
const chai = require('chai')
const expect = chai.expect
const should = chai.should()
const chaiHttp = require('chai-http') 
chai.use(chaiHttp);

const postObject = {
    "user": {
        "sessionid":"923719602972808660095992479824"
    },
    "data": {
        "id": "2201",
        "name": "Sample_Test_Name12",
        "email": "Sample_Test_Name121@test.com"
    }
}

describe('Login Check', function () {
    it('Should Return status code of 401 as no session ID is passed', (done) => {
        chai.request('http://localhost:3004/master/read')
            .post('/master/read')
            .end((err,res) => {
                expect(res).to.have.status(401);
                done()
            })
    });

    it('Should Return status code of 200 when correct session id is passed', (done) => {
        chai.request('http://localhost:3004')
            .post('/master/read').send(postObject)
            .end((err,res) => {
                expect(res).to.have.status(200);
                done()
            })
    });
});


describe('master/Read', function () {
    it('Should return an Array with status code 200 ', (done) => {
        chai.request('http://localhost:3004')
            .post('/master/read').send(postObject)
            .end((err,res) => {
                expect(res).to.have.status(200);
                res.body.should.be.a('array');
                done()
            })
    });
});

describe('master/create', function () {
    it(' Should return status code 200 and rows affected as 1', (done) => {
        chai.request('http://localhost:3004')
            .post('/master/create').send(postObject)
            .end((err,res) => {
                expect(res).to.have.status(200);
                res.body.should.be.a('object');
                res.should.have.property('error')
                assert.equal(res.error,false)
                assert.equal(JSON.stringify(res.body),JSON.stringify({"rowsAffected": 1}))
                done()
            })
    });
});

describe('master/update', function () {
    it(' Should return status code 200 and rows affected as 1', (done) => {
        chai.request('http://localhost:3004')
            .post('/master/update').send(postObject)
            .end((err,res) => {
                expect(res).to.have.status(200);
                res.body.should.be.a('object');
                res.should.have.property('error')
                assert.equal(res.error,false)
                assert.equal(JSON.stringify(res.body),JSON.stringify({"rowsAffected": 1}))
                done()
            })
    });
});

describe('master/delete', function () {
    it(' Should return status code 200 and rows affected as 1', (done) => {
        chai.request('http://localhost:3004')
            .post('/master/delete').send(postObject)
            .end((err,res) => {
                expect(res).to.have.status(200);
                res.body.should.be.a('object');
                res.should.have.property('error')
                assert.equal(res.error,false)
                assert.equal(JSON.stringify(res.body),JSON.stringify({"rowsAffected": 1}))
                done()
            })
    });
});
