const MongoClient = require("mongodb").MongoClient;
const oracledb = require("oracledb");
oracledb.outFormat = oracledb.OBJECT;
// oracledb.poolMin = 5;
// oracledb.poolPingInterval = 10;
// oracledb.poolTimeout = 10;
oracledb.autoCommit = true;
logger.info("Oracle client library version is " + oracledb.oracleClientVersionString);
const _ = require("lodash");
const logincheck = require("./logincheck");

async function getAlterDateFormat(conn) {
  let connection = null;
  try {
    connection = await oracledb.getConnection({
      user: conn.user,
      password: conn.password,
      connectString: conn.connectString
    });
    const result = await connection.execute(`select  paramvalue FROM parameter WHERE Upper(paramname) LIKE 'ORACLESHORTDATEFORMAT'`);
    return result.rows[0].PARAMVALUE;
  } catch (error) {
    logger.error(`Init: Error while getAlterDateFormat: ${((error.stack) ? error.stack : error)}`)
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        logger.error(`Init: Error while closing connection : ${err}`);
      }
    }
  }
}

module.exports = async (app, dbconfig) => {
  // MONGO
  if (dbconfig.userDB) {
    try {
      let aclient = await MongoClient.connect(dbconfig.userDB.sessionURL, {
        useNewUrlParser: true,
        poolSize: 10,
        connectTimeoutMS: 3000,
        socketTimeoutMS: 3000
      });
      app.locals.userDB = aclient.db(dbconfig.userDB.sessionDB);
    } catch (error) {
      logger.error("Init: Error connecting to Mongodb -  \n" + error);
      // throw error;
    }
  } else {
    logger.error("Init: No user Mongodb credentials provided!");
    // throw "No user DB credentials provided!";
  }

  // For Multiple Mongo connections 
  app.locals.DBPool = {};
  if (dbconfig.mongodb) {
    for (let index = 0; index < dbconfig.mongodb.length; index++) {
      const item = dbconfig.mongodb[index];
      try {
        let aclient = await MongoClient.connect(item.sessionURL, {
          useNewUrlParser: true,
          poolSize: 10
        });
        app.locals.DBPool[item.alias] = aclient.db(item.sessionDB);
        logger.info("Init: Mongo connected");
      } catch (error) {
        logger.error("Init: Error connecting to database : " + item.alias + " - " + error);
        throw error;
      }
    }
  }

  if (dbconfig.oracledb) {
    dbconfig.DATE_FORMAT = dbconfig.DATE_FORMAT || "MM/DD/YYYY";
    for (let index = 0; index < dbconfig.oracledb.length; index++) {
      const item = dbconfig.oracledb[index];
      let connection = null;
      try {
        //let alterdateformat = await getAlterDateFormat(item);
        let connpool = await oracledb.createPool({
          user: item.user,
          password: item.password,
          connectString: item.connectString,
          // sessionCallback: async function (connection, requestedTag, cb) {
          //   let result = await connection.execute(
          //     "alter session set nls_date_format = '" + alterdateformat + "'"
          //   );
          //   cb();
          // }
        });
        app.locals.DBPool[item.alias] = connpool;
        // to check if db credentials are correct
        connection = await connpool.getConnection()
        logger.info("Init: Oracle Connected");
      } catch (error) {
        logger.error("Init: Error connecting to database : " + item.alias + " - " + error.stack);
      } finally {
        try {
          if (connection) await connection.close();
        } catch (error) {
          logger.error(`Init: Error while closing connection : ${error}`)
        }
      }
    }
  }
  global.DB = app.locals.DBPool;
  //logincheck(app);

  app.post("/ping", async (req, res) => {
    res.status(200).send({ "status": "I'm alive" })
  })

  app.post("/poolstatus", async (req, res) => {
    let poolstatus = {}
    for (let pool in app.locals.DBPool) {
      let dbpool = app.locals.DBPool[pool];
      poolstatus[pool] = {}
      poolstatus[pool]["status"] = dbpool.status;
      poolstatus[pool]["connectionsOpen"] = dbpool.connectionsOpen;
      poolstatus[pool]["connectionsInUse"] = dbpool.connectionsInUse;
      poolstatus[pool]["_totalConnectionRequests"] = dbpool._totalConnectionRequests;
      poolstatus[pool]["_totalFailedRequests"] = dbpool._totalFailedRequests;
      poolstatus[pool]["_totalRequestTimeouts"] = dbpool._totalRequestTimeouts;
    }
    res.status(200).send(poolstatus);
  })
};
