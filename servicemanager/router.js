const fs = require("fs");
const path = require("path");
var express = require("express");
const getDirectories = source =>
  fs.readdirSync(source)
    .map(name => name)
    .filter(name =>
      fs.lstatSync(path.join(source, name))
        .isDirectory()
    );

module.exports = async (app, socket, servicespath) => {
  var subapps = getDirectories(servicespath);
  for (let index = 0; index < subapps.length; index++) {
    const element = subapps[index];
    let elpath = path.join(servicespath, element);

    // why find init in services?
    if (fs.existsSync(path.join(elpath, "init.js")))
      await require(path.join(elpath, "init.js"))(app);

    let router = express.Router();
    let nsp = socket.of("/" + element);
    nsp.on("connection", function (socket) {
      logger.info("Router: someone connected");
    });
    fs.readdirSync(elpath).forEach(function (file) {
      if (fs.lstatSync(path.join(elpath, file)).isDirectory()) {
        if (fs.existsSync(path.join(elpath, file, "routes.js")))
          require(path.join(elpath, file, "routes.js"))(router, app);
        if (fs.existsSync(path.join(elpath, file, "events.js"))) {
          nsp.on("connection", (socket) => require(path.join(elpath, file, "events.js"))(socket, app));
        }
      }
    });
    app.use("/" + element, router);
  }
  return app;
};
