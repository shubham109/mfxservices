# Services framework

To allow creation of services in a modularised way.  
Should allow each service to be developed and tested independently.  

- Framework uses **Expressjs** for node and API of Expressjs is applicable in terms of Request and Response objects.  
- Framework supports hosting multiple apps.  
- So services to be organised under separate apps.  
- Each app has a root path.  
- Framework also provides database connections.
- Mongodb conection is added to app.locals collection. 
- Similary Oracle will also be added. 

A service creator function takes one parameter Express router and is expected to add the end points to the router.  
Please refer to https://expressjs.com/en/4x/api.html#router.METHOD for documentation of how to create a route callback function.  
Framework then adds these services to the server under root which is same as the directory name of the app.  

## Pre-requisite

 - Oracle Client Library - [Follow this instructions](https://oracle.github.io/node-oracledb/INSTALL.html#364-install-the-free-oracle-instant-client-zip)

## How to use this Framework
> This Framework/repository will act as a submodule for your project.  
> **Hosted** the below sample project [here](https://gitlab.com/mukeshsuthar/samplefunds)
### Sample Test Project
1. Create a Sample REST Project, lets call it as sampleFunds.
2. Clone this repository inside your sampleFunds project by using `git clone https://gitlab.com/avijaysimha/servicemanager/`
3. Create a **Folder Structure** shown as below:
   ```
   sampleFunds/
    ├─ fundsnservices/ 
    │   └─ master/              <--- Service route
    │       └─ home/            
    │           └─ routes.js    <--- This is where you specify endpoints - code here.
    │   └─ dbconfig.js          <--- Database connection config
    ├─ servicemanager/          <--- This Framework
    │   └─ ...
    └─ index.js
   ```
4. File templetes
   1. **index.js**
        ```
        const path = require("path");
        const servicemgr = require("./servicemanager");
        var port = 3000;
        (async () => {
            let server = await servicemgr.initServer({
                path: path.join(__dirname, "fundsnservices") // <-- Notable changes   
            });
            server.listen(port, () => {
                logger.info(`Services center listening on port ${port}!`);
            });
        })();
        ```
    2. **dbconfig.js**
        ```
        module.exports = {
        // MONGO Config.
        userDB: { sessionURL: "mongodb://127.0.0.1:27017", sessionDB: "scdb" }, 
        // ORACLE Config.
        oracledb: [
            {
            alias: "oraclePool",
            user: "viz",
            password: "vijay",
            connectString: "XE"
            }
        ]
        };
        ```
    3. **routes.js**
        ```
        var expressApp = null;
        module.exports = (router, app) => {
        expressApp = app;
        
        router.post("/getSampleText", async (req, res) => {
            // Oracle connections are stored in "app.locals.DBPool" variable
            // Mongo connections are stored in "app.locals.userDB" variable
            // code logic here
            return res.json({"data":"sample text"});
        });
        };
        ```
5. Starting service center
    

### Testing fundsnservices
1. Login to access the endpoints
   - inside postman, send **POST** request for **127.0.0.1:3000/login**
   - with body payload as  
   ```
    {
        "user_id":"123",
        "session_id":"abc1234",
        "app_id":"xyz123"
    }
   ```
2. Access Endpoints
   - Goto postman and send **POST** request for **127.0.0.1:3000/master/getSampleText**
   - with body payload as 
   ```
   {
        "user": {
            "user_id": "123",
            "session_id": "abc1234",
            "app_id": "xyz123"
        }
    }   
   ```
   - You'll get `{"data":"sample text"}` in response
