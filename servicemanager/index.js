const path = require("path");
const logger = require("./logger");
const decrypt = require("./decrypt")
const express = require("express");
const app = express();
const helmet = require("helmet");
const bodyParser = require("body-parser");
let server = require("http").Server(app);
let io = require("socket.io")(server);
let cors = require('cors')
global.logger = logger;
global.config = require('../MFXservices/dbconfig');

app.use(cors());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.hidePoweredBy());
app.use(helmet.contentSecurityPolicy({
  directives: {
    defaultSrc: ["'self'"],
    styleSrc: ["'self'", "'unsafe-inline'"],
    scriptSrc: ["'self'", "'unsafe-inline'", "'unsafe-eval'"],
    // sandbox: ["allow-forms", "allow-scripts"],
    reportUri: "/report-violation",
    objectSrc: ["'none'"],
    upgradeInsecureRequests: true,
    workerSrc: false
  }
})
);
app.use(bodyParser.json());
app.use(rTracer.expressMiddleware())

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,Content-Type,Content-Length, Authorization, Accept,X-Requested-With"
  );
  res.header("Access-Control-Allow-Methods", "POST,GET");
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});
// app.use(require('express-status-monitor')());
app.use(require("morgan")(":remote-addr :remote-user :response-time ms - :method :url - :status :referrer :user-agent ", { "stream": logger.stream }));

app.post("/report-violation", (req, res) => {
  if (req.body) {
    logger.error("ServiceManager: CSP Violation: " + JSON.stringify(req.body));
  } else {
    logger.error("ServiceManager: CSP Violation: No data received!");
  }

  res.status(204).end();
});

app.get("/", async (req, res) => {
  logger.info("inside root endpoint");
  res.status(200).send("Home Page");
})

process.on('SIGINT', () => {
  if (global.ch) { logger.info("ServiceManager: Closing mq channel"); ch.close() }
  if (global.conn) { logger.info("ServiceManager: Closing db connections"); conn.close() };
  if (global.DB["oraclePool"]) { logger.info("ServiceManager: Closing pool"); DB["oraclePool"].close(0) };
  if (server) { logger.info("ServiceManager: Closing express server"); server.close(); }
  process.exit()
});


module.exports = {
  initServer: async opt => {
    try {
      logger.debug(`config: ${JSON.stringify(config)}`);
      await require("./init")(app, config);
      await require("./router")(app, io, opt.path);
      await require("./loadfunctions")(path.join(__dirname, 'utilityFunctions'));
    } catch (error) {
      logger.error("ServiceManager: Error occurred : " + error);
    }
    return server;
  }
};
