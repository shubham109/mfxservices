var amqp = require("amqplib");
global.conn = null;
global.ch = null;

function bail(err) {
    process.exit(1);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function retry(maxRetries, fn, dbName) {
    return fn().catch(async function (err) {
        logger.error(`Init : error in ${dbName} connection: ${err}: Retrying...`);
        await sleep(1000)
        if (maxRetries < 0) {
            logger.error("Max Retries reached.");
            bail();
        }
        return retry(maxRetries - 1, fn, dbName);
    });
}

async function connectToRMQ() {
    try {
        conn = await amqp.connect({
            protocol: "amqp",
            hostname: config.rabbitmq.serverip,
            port: config.rabbitmq.port || 5672,
            username: config.rabbitmq.user,
            password: config.rabbitmq.password,
            locale: "en_US",
            frameMax: 0,
            heartbeat: 0,
            vhost: "/"
        })
        ch = await conn.createChannel()
        await ch.assertExchange(config.rabbitmq.exchange, "topic", { durable: true })
        logger.info(`Rabbitmq: Connected.`)
    } catch (error) {
        if (ch) { ch.close() }
        if (conn) { conn.close() };
        throw (`Rabbitmq: ${error}`)
    }
    return null;
}

module.exports = async () => {
    await retry(2, connectToRMQ, "Rabbitmq");
    return async (msg, key) => {
        logger.info(`Publishing to ${config.rabbitmq.exchange} with key ${key} and message: ${JSON.stringify(msg)}`)
        await ch.publish(config.rabbitmq.exchange, key, Buffer.from(JSON.stringify(msg)));
        return null;
    }
}
