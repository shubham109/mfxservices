module.exports = async (library) => {
    if(config.mq){
        library.add('sendToMq', await require(`./${config.mq}`)());
    }else{
        logger.warn(`Mq: no Mq specified in config.`)
    }
    return null;
}