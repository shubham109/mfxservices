const _ = require("lodash");

module.exports = app => {
  const validateSessionMongo = async function (user) {
    try {
      let data = await app.locals.userDB.collection("validSessions").findOne({
        userid: { $in: [user.userid] },
        sessionid: { $in: [user.sessionid] },
        appid: { $in: [user.appid] }
      });
      if (data == null) {
        logger.warn("logincheck: no data found while validating session in mongo");
        return false;
      } else {
        return true;
      }
    } catch (error) {
      logger.error("logincheck: Mongo: Error while validating user: " + error);
      return false;
    }
  };

 // const oracleUserQry = `SELECT (timeinterval-diff) timediff ,u.* FROM (select Round(To_Number((SYSDATE - LAST_ACCESSTIME) * 24 * 60)) as diff,decode((SELECT Count(*) FROM iwz_parameter WHERE paramname='timeout_interval'),0,60,(SELECT paramvalue FROM iwz_parameter WHERE paramname='timeout_interval')) timeinterval,u.USER_ID, u. USER_NAME,u. BRANCH_CODE,u. PROFILE_ID,u. USER_SHORT_CODE,u. SESSIONID,u. LOCATION,u. COUNTER,u. REPORTS_TO,u. ENTITY_CODE,u.isadmin from iwz_user_master u where sessionid = :sessionid and active='Y')u`;
  const validateSessionOracle = async function (userObj) {
    let connection = null;
    try {
      connection = await app.locals.DBPool["oraclePool"].getConnection();
      let result = await connection.execute(oracleUserQry, { sessionid: userObj.sessionid })
      if (result.rows.length == 0) {
        logger.warn("logincheck: no data found while validating session in oracle");
        return false;
      } else {
        userObj.user_id = result.rows[0].USER_ID;
        userObj.user_name = result.rows[0].USER_NAME;
        return true;
      }
    } catch (error) {
      logger.error("logincheck: Oracle: Error while validating user: " + error);
      return false;
    } finally {
      try {
        if (connection) await connection.close();
      } catch (error) {
        logger.error(`Init: Error while closing connection : ${error}`)
      }
    }
  }

  app.use(async (req, res, next) => {
    try {
      let userObj = null;
      if (req.headers.sessionid) { // check sessionid in headers
        userObj = {
          sessionid: req.headers.sessionid
        };
      } else if (req.body.user && req.body.user.sessionid) { // if not in header, check in user obj
        userObj = req.body.user;
      }
      else { // no session id found
        res.status(401).send({ message: "No sessionid passed" });
        logger.error(`logincheck: No session id passed.`)
        return false;
      }
      if (await validateSessionOracle(userObj || {})) {
        req.body.user = userObj;
        next();
      } else {
        res.status(401).send({ message: `Invalid Session for id: ${userObj.sessionid}` });
      }
    } catch (error) {
      logger.error(`logincheck: Error: ${error.stack}`);
      res.status(401).send(`logincheck: Error: ${error.stack}`);
      return false;
    }
  });
  app.locals.validateSessionMongo = validateSessionMongo; // for backward compatibility
  app.locals.validateSessionOracle = validateSessionOracle; // for backward compatibility
};
