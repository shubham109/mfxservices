const path = require("path");
const fs = require("fs");

global.funclib = {};

module.exports = async foldername => {
  let funcmodules = fs.readdirSync(foldername);
  for (let i = 0; i < funcmodules.length; i++) {
    let filepath = path.join(foldername, funcmodules[i]);
    await require(filepath)({
      add(name, funccode) {
        funclib[name] = funccode;
      }
    });
  }
  return null;
};
