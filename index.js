const path = require("path");
const servicemgr = require("./servicemanager");
var port = 3004;
(async () => {
    let server = await servicemgr.initServer({
        path: path.join(__dirname, "MFXservices") //< --Notable changes
    });
    server.listen(port, () => {
        logger.info(`Server: Services center listening on port ${port}!`);
    });
})();